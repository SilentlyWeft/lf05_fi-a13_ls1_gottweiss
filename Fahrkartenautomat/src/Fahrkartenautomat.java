﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int anzahlTickets;
       double ticketEinzelpreis;
       
       System.out.print("Preis pro Ticket in (EURO): ");
       ticketEinzelpreis = tastatur.nextDouble();
       
       System.out.print("Wie viele Tickets werden gekauft?: ");
       anzahlTickets = tastatur.nextInt();
       
       
       zuZahlenderBetrag = anzahlTickets * ticketEinzelpreis;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.printf("%s%.2f%s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag)," EURO ");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrscheine werden ausgegeben.");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}
/* Der Datentyp muss ein Integer sein, da es nur ganze Tickets gibt
 * zum Beispiel macht es keinen Sinn ein dreiviertel Ticket zu kaufen

* in Zeile 23 wird "anzahlTickets" mit "ticketEinzelpreis" 
* multipliziert, dadurch ist das Ergebnis "zuZahlenderBetrag" 
* vorlaeufig eine Gleitkommazahl, da "ticketEinzelpreis" eine Gleitkommazahl ist
* und somit auch zu einer Gleitkommazahl wird 
*/